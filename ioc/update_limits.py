import subprocess

with open("./autosave/THCCS/save/settings.sav0", 'r') as f:
    lines = f.readlines()
    for l in lines:
        if l[:3] == "Tgt":
            cmd = "caput {}" .format(l)
            try:
                ret_code = subprocess.check_call(cmd, shell=True)
            except Exception as e:
                print(e)

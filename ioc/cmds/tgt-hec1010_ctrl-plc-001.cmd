# require iocStats,ae5d083
# rem require s7plc,1.4.0p
# rem require asyn,4.33.0
# rem require modbus,2.11.0p
# rem require autosave,5.9.0
# rem require calc,3.7.1

# require s7plc,1.4.1
# require asyn,4.36.0
# require modbus,3.0.0
# require autosave,5.10.0
# require calc,3.7.3

#require s7plc,1.4.1
#require asyn,4.33.0
#require modbus,2.11.0p
#require calc,3.7.1
#require autosave,5.10.0

require s7plc
require asyn
require modbus
require calc
require autosave

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")
epicsEnvSet("DB", "$(TOP)/db")
epicsEnvSet("AS", "$(TOP)/autosave"))
epicsEnvSet("IOCNAME", "THCCS")
epicsEnvSet("PLCNAME","tgt-hec1010_ctrl-plc-001")
# epicsEnvSet("IPADDR","192.168.0.100") # X1 InterfaceID 64 in _CommsEPICS in PLC
epicsEnvSet("IPADDR","172.30.4.163") # X2 InterfaceID 73 in _CommsEPICS in PLC
epicsEnvSet("RECVTIMEOUT","3000")
epicsEnvSet("S7DRVPORT","503")
epicsEnvSet("MODBUSDRVPORT","502")
epicsEnvSet("INSIZE","4766")
epicsEnvSet("BIGENDIAN","1")

# PLC communication
s7plcConfigure($(PLCNAME), $(IPADDR), $(S7DRVPORT), $(INSIZE), 0, $(BIGENDIAN), $(RECVTIMEOUT), 0)
drvAsynIPPortConfigure($(PLCNAME), $(IPADDR):$(MODBUSDRVPORT), 0, 0, 1)
modbusInterposeConfig($(PLCNAME), 0, $(RECVTIMEOUT), 0)
drvModbusAsynConfigure("$(PLCNAME)write", $(PLCNAME), 0, 16, -1, 2, 0, 0, "S7-1500")

# Load plc interface database
dbLoadRecords("$(DB)/$(PLCNAME).db", "PLCNAME=$(PLCNAME), MODVERSION=1.0, S7_PORT=$(S7DRVPORT), MODBUS_PORT=$(MODBUSDRVPORT)")

# loadIocsh("iocStats.iocsh", "IOCNAME=$(IOC)")
iocshLoad("$(autosave_DIR)/autosave.iocsh", "IOCNAME=$(IOCNAME), AS_TOP=$(AS)")

iocInit()

dbl > "$(TOP)/${IOCNAME}_PVs.list"

CCDB_PLC="Tgt-HeC1010:Ctrl-PLC-001"
E3_NAME="tgt-hec1010_ctrl-plc-001"
PLCF_PATH="/home/${USER}/plcfactory"
DIR_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
DB_FILE_PATH=${DIR_PATH}/ioc/db/${E3_NAME}.db
CMD_FILE_PATH=${DIR_PATH}/ioc/cmds/${E3_NAME}.cmd

function print() {
color_green=$'\033[32;1m'
color_end=$'\033[0m'
printf "%s%s%s\n" "$color_green" "$1"  "$color_end"
}

# Pull all changes
print "Pulling thccs"
git pull

print "cd into ${PLCF_PATH}"
cd ${PLCF_PATH}
print "Pulling plcfactory"
git pull

# Delete currently prevailing output from plcfactory
print "Deleting old plcf output dir"
rm -r output/

# Run plcfactory
print "Running plcfactory"
python2 plcfactory.py -d ${CCDB_PLC} --plc-interface=14 --e3=${E3_NAME} --plc-diag

print "cd back into thccs"
cd ${DIR_PATH}

# Update S7 input block size in .cmd file
print "Updating S7 input block size in .cmd file"
while IFS='' read -r line || [[ -n "$line" ]]; do
    if [[ $line == *"\"INSIZE\""* ]]; then
        OLD_LINE=$line
        OLD_MSG_SIZE=$(echo $line | egrep "INSIZE" | egrep -o "[0-9]+")
    fi
done < ${CMD_FILE_PATH}

while IFS='' read -r line || [[ -n "$line" ]]; do
    if [[ $line == *"Input block size"* ]]; then
        NEW_MSG_SIZE=$(echo $line | egrep -o "[0-9]+")
    fi
done < ${PLCF_PATH}/output/${E3_NAME}/modules/e3-${E3_NAME}/${E3_NAME}/iocsh/${E3_NAME}.iocsh
NEW_LINE=$(echo "${OLD_LINE/${OLD_MSG_SIZE}/${NEW_MSG_SIZE}}")

# Verify non-empty NEW_LINE
if [[ $NEW_LINE == *"\"\""* ]]; then # If NEW_LINE contains '""' it has failed
    echo -e "\nplcfactory failed, check output."
    exit 1
fi

sed -i "s/${OLD_LINE}/${NEW_LINE}/g" ${CMD_FILE_PATH}

echo -e "Updated:\n${OLD_LINE} to\n${NEW_LINE}"

# Fetch new db file from plcfactory
print "Fetching new db file from plcfactory"
#cp ${PLCF_PATH}/output/${E3_NAME}/modules/e3-${E3_NAME}/${E3_NAME}-loc/db/${E3_NAME}.db ${DIR_PATH}/ioc/db/
cp ${PLCF_PATH}/output/${E3_NAME}/modules/e3-${E3_NAME}/${E3_NAME}/db/${E3_NAME}.db ${DIR_PATH}/ioc/db/

# Fetch new source file from plcfactory
print "Fetching new PLCFactory_external_source_TIAv14.scl from plcfactory"
cp ${PLCF_PATH}/output/${E3_NAME}/PLCFactory_external_source_TIAv14.scl ${DIR_PATH}/plc/
cp ${PLCF_PATH}/output/${E3_NAME}/PLCFactory_external_source_standard_TIAv14.scl ${DIR_PATH}/plc/

# Git control
cd ${DIR_PATH}
print "Git adding .db, .cmd, PLCFactory_external_source_TIAv14.scl and PLCFactory_external_source_standard_TIAv14.scl"
git add ${DB_FILE_PATH} ${CMD_FILE_PATH} ${DIR_PATH}/plc/PLCFactory_external_source_TIAv14.scl ${DIR_PATH}/plc/PLCFactory_external_source_standard_TIAv14.scl
print "Commiting"
git commit -m "automatic commit after using plcfactory"
print "Pushing"
git push

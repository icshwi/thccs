<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>CMS Y1 - CV-42310 Control</name>
  <macros>
    <PLCName>CrS-CMS:Cryo-PLC-01</PLCName>
  </macros>
  <width>1780</width>
  <height>1100</height>
  <background_color>
    <color name="WHITE" red="255" green="255" blue="255">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>Y10 - V-001a/b and YSV-005a/b</text>
    <y>90</y>
    <width>1780</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="BLUE-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <background_color>
      <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel</name>
    <text>State Flag Status</text>
    <x>810</x>
    <y>134</y>
    <width>570</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel</name>
    <text>State Block Icon</text>
    <x>1410</x>
    <y>136</y>
    <width>330</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1</name>
    <macros>
      <Description>Preparation Purge Pressure</Description>
      <EngUnit>bara</EngUnit>
      <ParameterID>S1</ParameterID>
    </macros>
    <file>../../../../99-Shared/StateMachines/blockicons/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>173</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter2</name>
    <macros>
      <Description>Preparation Transition Pressure</Description>
      <EngUnit>bara</EngUnit>
      <ParameterID>S2</ParameterID>
    </macros>
    <file>../../../../99-Shared/StateMachines/blockicons/StateMachine_Paramter_Template.bob</file>
    <x>30</x>
    <y>209</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1</name>
    <macros>
      <Description>CV-42310 finished</Description>
      <FlagID>Flag1</FlagID>
    </macros>
    <file>../../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>173</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ControlledDevicesLabel</name>
    <text>Controlled / Measured Devices</text>
    <x>30</x>
    <y>560</y>
    <width>1710</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_DevicesInManual</name>
    <text>Warning: some controlled devices are in Manual!</text>
    <x>30</x>
    <y>500</y>
    <width>501</width>
    <height>35</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="24.0">
      </font>
    </font>
    <foreground_color>
      <color name="RAL-2000" red="213" green="111" blue="1">
      </color>
    </foreground_color>
    <horizontal_alignment>1</horizontal_alignment>
    <auto_size>true</auto_size>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>${StepDeviceName}:DevInManual</pv_name>
      </rule>
    </rules>
    <border_width>3</border_width>
    <border_color>
      <color name="ORANGE" red="254" green="194" blue="81">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel_1</name>
    <text>Parallel State Parameters</text>
    <x>30</x>
    <y>134</y>
    <width>759</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_1</name>
    <macros>
      <Description>Failure Action Active</Description>
      <FlagID>Flag2</FlagID>
    </macros>
    <file>../../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>208</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>../Embedded/THCCS_Header.bob</file>
    <width>2080</width>
    <height>90</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Y1</name>
    <macros>
      <Faceplate>../../../../CMS_OPI_MASTER/OPI/States/CMS_ParallelFaceplate_Y1.bob</Faceplate>
      <StepName>Y10 V-001a/b &amp; YSV-005a/b</StepName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>010</WIDIndex>
      <WIDSecSub>Tgt-HeC1010</WIDSecSub>
    </macros>
    <file>../../../../99-Shared/StateMachines/blockicons/StateMachine_BlockIcon_Parallel_Compact.bob</file>
    <x>1410</x>
    <y>175</y>
    <width>330</width>
    <height>130</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel_2</name>
    <text>Parallel Function Internal Alarms</text>
    <x>810</x>
    <y>380</y>
    <width>570</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_2</name>
    <macros>
      <AlarmID>Alarm1</AlarmID>
      <Description>All parameters are zero!</Description>
    </macros>
    <file>../../../../99-Shared/StateMachines/blockicons/StateMachine_AlarmMessage_Template.bob</file>
    <x>810</x>
    <y>424</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>V-001a</name>
    <macros>
      <WIDDev>FIC</WIDDev>
      <WIDDev4>V</WIDDev4>
      <WIDDis>Proc</WIDDis>
      <WIDIndex>001</WIDIndex>
      <WIDIndex1>001a</WIDIndex1>
      <WIDSecSub>Tgt-HeC1010</WIDSecSub>
    </macros>
    <file>../ICS_OPI_LIBRARY/DeviceTypes/HeliumCirculators/circulator_V-001.bob</file>
    <x>30</x>
    <y>612</y>
    <width>200</width>
    <height>234</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>V-001a_1</name>
    <macros>
      <WIDDev>FIC</WIDDev>
      <WIDDev4>V</WIDDev4>
      <WIDDis>Proc</WIDDis>
      <WIDIndex>001</WIDIndex>
      <WIDIndex1>001b</WIDIndex1>
      <WIDSecSub>Tgt-HeC1010</WIDSecSub>
    </macros>
    <file>../ICS_OPI_LIBRARY/DeviceTypes/HeliumCirculators/circulator_V-001.bob</file>
    <x>570</x>
    <y>612</y>
    <width>200</width>
    <height>234</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>YSV-005a</name>
    <macros>
      <WIDDev>YSV</WIDDev>
      <WIDDis>Proc</WIDDis>
      <WIDIndex>005a</WIDIndex>
      <WIDSecSub>Tgt-HeC1010</WIDSecSub>
    </macros>
    <file>../ICS_OPI_LIBRARY/DeviceTypes/Valves/PV_VALVE_AUMA_BlockIcon_Horizontal_Compact.bob</file>
    <x>230</x>
    <y>612</y>
    <width>100</width>
    <height>160</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>YSV-005b</name>
    <macros>
      <WIDDev>YSV</WIDDev>
      <WIDDis>Proc</WIDDis>
      <WIDIndex>005b</WIDIndex>
      <WIDSecSub>Tgt-HeC1010</WIDSecSub>
    </macros>
    <file>../ICS_OPI_LIBRARY/DeviceTypes/Valves/PV_VALVE_AUMA_BlockIcon_Horizontal_Compact.bob</file>
    <x>770</x>
    <y>612</y>
    <width>100</width>
    <height>160</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
</display>

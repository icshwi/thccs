import time

from opc_client import OPCClient

class YSV(object):
    def __init__(self, client, energized, opened, closed):
        self.client = client
        self.energized = energized
        self.opened = opened
        self.closed = closed

    def run(self):
        oc = [self.opened, self.closed]
        energized = self.energized
        name = energized.get_display_name().Text
        if "open" in name:
            check = 0
        else:
            check = 1

        while True:
            if energized.get_value() and not oc[check].get_value():
                self.client.setValue(oc[1-check], False)
                self.client.setValue(oc[check], True)

            if not energized.get_value() and not oc[1-check].get_value():
                self.client.setValue(oc[check], False)
                self.client.setValue(oc[1-check], True)

            time.sleep(0.5)

    def get_name(self):
        return self.energized.get_display_name().Text.split("_")[1]

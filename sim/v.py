import time
import random
import struct

from epics import ca, caget, caput, cainfo, PV
from opc_client import OPCClient

class V(object):
    def __init__(self, client, speed_sp, speed_fb, control_bits):
        self.client = client
        self.speed_sp = speed_sp
        self.speed_fb = speed_fb
        self.control_bits = control_bits
        self.stopping = False

    def run(self):
        if self.speed_fb.get_value() >= 10000:
            op_state = 2
            speed = self.speed_fb.get_value()
        else:
            speed = 0
            op_state = 0

        ramp_rate = 1000
        while True:
            bits = self.control_bits.get_value()
            cmd_run = bin(bits)[-2] == "1" # The second to last bit is the "command run"

            if op_state == 0 and cmd_run:
                op_state = 1

            if op_state == 1:
                speed = speed + ramp_rate
                if speed >= 10000:
                    speed = 10000
                    op_state = 2

                self.client.setValue(self.speed_fb, speed)

            # Running
            if op_state == 2:
                sp = self.speed_sp.get_value()
                if sp < 0:
                    sp = sp + 2**16 # Compensate for opcua conversion bullshit

                self.client.setValue(self.speed_fb, sp)

                if not cmd_run:
                    op_state = 3

            # Stopping
            if op_state == 3:
                speed = speed - ramp_rate
                if speed <= 0:
                    speed = 0
                    op_state = 0

                self.client.setValue(self.speed_fb, speed)
                time.sleep(0.1)

            time.sleep(0.2)

    def get_name(self):
        return self.control_bits.get_display_name().Text.split("_")[1]

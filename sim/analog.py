import time
import random

__author__ = "Johannes Kazantzidis"
__email__ = "johannes.kazantzidis@ess.eu"
__status__ = "Production"

class Analog(object):
    def __init__(self, client, node):
        self.client = client
        self.ai = node

    def run(self):
        if "PT-007" in self.get_name():
            pcv14 = self.client.get_root_node().get_child(
                ['0:Objects', '3:THCCS_PLC', '3:Inputs', '3:hwi_PCV-014'])
            pcv15 = self.client.get_root_node().get_child(
                ['0:Objects', '3:THCCS_PLC', '3:Inputs', '3:hwi_PCV-015'])
            value = 0
        elif self.get_name() in ["TT-002", "TT-003", "TT-004", "TT-005"]:
            bp = self.client.get_root_node().get_child(
                ['0:Objects', '3:THCCS_PLC', '3:DataBlocksGlobal', '3:external_signals',
                     '3:beam_power_mw'])
        elif "FT-001a" in self.get_name():
            ft = self.client.get_root_node().get_child(
                ['0:Objects', '3:THCCS_PLC', '3:Inputs', '3:hwica_speed'])
        elif "FT-001b" in self.get_name():
            ft = self.client.get_root_node().get_child(
                ['0:Objects', '3:THCCS_PLC', '3:Inputs', '3:hwicb_speed'])

        sleep = 0.5

        while True:
            if "TT-002" in self.get_name():
                value = 20 + bp.get_value()*45 # * (1 - speed/80000)
                tl = 0
                th = 300
                td = value - tl
                value = int(td * 27648 / (th - tl))
            elif self.get_name() in ["TT-003", "TT-004", "TT-005", "PT-004", "PT-005", "PT-007"]:
                continue
            # elif "TT-003" in self.get_name():
            #     # value = float(bp.get_value())*0.58*3600 + 2000
            #     continue
            # elif "TT-004" in self.get_name():
            #     # value = float(bp.get_value())*0.31*3600 + 2000
            #     continue
            elif "TT-005" in self.get_name():
                value = float(bp.get_value())*0.17*3600 + 2000
            elif self.get_name() in ["TT-011", "TT-012"]:
                value = random.randint(13500, 14000)
            elif self.get_name() in ["TT-021", "TT-022"]:
                value = random.randint(9000, 9200)
            elif "RT" in self.get_name():
                value = random.randint(1000, 12000)
            elif "PT-007" in self.get_name():
                factor = 30
                sleep = 0
                value += pcv15.get_value()/factor - pcv14.get_value()/factor
                if value > 27648:
                    value = 27648
            elif self.get_name() in ["PT-011", "PT-012", "PT-014", "PT-015", "PT-031"]:
                value = random.randint(10400, 10600)
            elif "FT-001" in self.get_name():
                value = float(ft.get_value()) * 0.3
                if value > 0:
                    value = 1000 + value
                else:
                    value = 0
            else:
                # break
                value = random.randint(13000, 14000)

            self.client.setValue(self.ai, int(value))
            time.sleep(sleep)

    def get_name(self):
        return self.ai.get_display_name().Text.split("_")[1]

import time

from opc_client import OPCClient

class CV(object):
    def __init__(self, client, cmd_openess, fb_openess):
        self.client = client
        self.cmd_openess = cmd_openess
        self.fb_openess = fb_openess

    def run(self):
        while True:
            self.client.setValue(self.fb_openess, self.cmd_openess.get_value())

            # time.sleep(0.2)

    def get_name(self):
        return self.fb_openess.get_display_name().Text.split("_")[1]

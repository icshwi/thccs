import argparse
import threading

from opc_client import OPCClient
from analog import Analog
from cv import CV
from v import V
from auma_pv import AumaPV

class SimThread(threading.Thread):
    def __init__(self, threadID, name, sim):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.sim = sim

    def run(self):
        print("Starting " + self.name)
        self.sim.run()
        print("Exiting " + self.name)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Simulator")
    parser.add_argument("ip", type=str, help="plc ip address")
    args = parser.parse_args()

    # Create and connect client
    client = OPCClient("opc.tcp://" + args.ip + ":4840")
    client.connect()

    # Get all TT, PT and RT nodes
    objects = client.get_root_node().get_child("0:Objects").get_children()
    plc = objects[-1]                    # Node for PLC
    inputs = plc.get_child("3:Inputs")   # Node for plc inputs
    outputs = plc.get_child("3:Outputs") # Node for plc outputs

    bp = client.get_root_node().get_child(
        ['0:Objects', '3:THCCS_PLC', '3:DataBlocksGlobal', '3:external_signals',
             '3:beam_power_mw'])


    analog_instruments = []                       # List of analog nodes
    valves = []
    pid_tags = []
    for node in inputs.get_children():
        node_name = node.get_display_name().Text  # Node name
        try:
            pid_tag = node_name.split("_")[1]  # P&ID tag
        except IndexError as e:
            continue

        if pid_tag in pid_tags:
            continue

        if pid_tag.split("-")[0] in ["TT", "PT", "FT", "RT"]:
            analog_instruments.append(Analog(client, node))
        elif "YSV" in node_name:
            cmd_o_node = outputs.get_child("hwo_" + pid_tag + "_open") # 'open'
            cmd_c_node = outputs.get_child("hwo_" + pid_tag + "_close")# 'close'
            o_node = inputs.get_child("hwi_" + pid_tag + "_opened") # 'opened'
            c_node = inputs.get_child("hwi_" + pid_tag + "_closed") # 'closed'
            valves.append(AumaPV(client, cmd_o_node, cmd_c_node, o_node, c_node))
        elif "CV-" in node_name:
            o_node = outputs.get_child("hwo_" + pid_tag) # 'open'
            i_node = inputs.get_child("hwi_" + pid_tag)  # 'openness'
            valves.append(CV(client, o_node, i_node))

        pid_tags.append(pid_tag)

    # Get circulator nodes
    circulators = [
        V(client,
              outputs.get_child("hwoca_speed"),
              inputs.get_child("hwica_speed"),
              outputs.get_child("hwoca_system_control_bits")),
        V(client,
              outputs.get_child("hwocb_speed"),
              inputs.get_child("hwicb_speed"),
              outputs.get_child("hwocb_system_control_bits")),
        ]

    # Create and start threads
    thread_id = 0
    for a in analog_instruments:
        SimThread(thread_id, a.get_name(), a).start()
        thread_id += 1

    for v in valves:
        SimThread(thread_id, v.get_name(), v).start()
        thread_id += 1

    for c in circulators:
        SimThread(thread_id, "circulator", c).start()
        thread_id += 1

import time

from opc_client import OPCClient

class AumaPV(object):
    def __init__(self, client, cmd_open, cmd_close, opened, closed):
        self.client = client
        self.cmd_open = cmd_open
        self.cmd_close = cmd_close
        self.opened = opened
        self.closed = closed

    def run(self):
        oc = [self.opened, self.closed]
        while True:
            print("{} - open: {}    close: {}".format(self.opened.get_display_name().Text.split("_")[1], self.cmd_open.get_value(), self.cmd_close.get_value()))
            if self.cmd_open.get_value() and not self.opened.get_value():
                self.client.setValue(self.closed, False)
                time.sleep(0.1)
                self.client.setValue(self.opened, True)


            if self.cmd_close.get_value() and not self.closed.get_value():
                self.client.setValue(self.opened, False)
                time.sleep(0.1)
                self.client.setValue(self.closed, True)

            time.sleep(0.2)

    def get_name(self):
        return self.opened.get_display_name().Text.split("_")[1]

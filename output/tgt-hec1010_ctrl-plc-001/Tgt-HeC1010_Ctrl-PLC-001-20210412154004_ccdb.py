#!/usr/bin/env python2

import os
import sys

sys.path.append(os.path.curdir)
sys.path.append(os.path.abspath(os.path.dirname(__file__)))

from ccdb_factory import CCDB_Factory

factory = CCDB_Factory()

# External links for ICS_CV
factory.addLink("ICS_CV", "EPI", "https://gitlab.esss.lu.se/icshwi/ics_library_definitions")

# External links for ICS_PID_ADV
factory.addLink("ICS_PID_ADV", "EPI", "https://gitlab.esss.lu.se/icshwi/ics_library_definitions")

# External links for ICS_ANALOG
factory.addLink("ICS_ANALOG", "EPI[ics_analog]", "https://gitlab.esss.lu.se/icshwi/ics_library_definitions")

# External links for THCCS_Circulator
factory.addLink("THCCS_Circulator", "EPI[def/thccs_circulator]", "https://gitlab.esss.lu.se/icshwi/thccs.git")

# External links for ICS_PV_AUMA
factory.addLink("ICS_PV_AUMA", "EPI", "https://gitlab.esss.lu.se/icshwi/ics_library_definitions")

# External links for ICS_PV
factory.addLink("ICS_PV", "EPI", "https://gitlab.esss.lu.se/icshwi/ics_library_definitions")

# External links for ICS_PID
factory.addLink("ICS_PID", "EPI", "https://gitlab.esss.lu.se/icshwi/ics_library_definitions")

#
# Adding PLC: Tgt-HeC1010:Ctrl-PLC-001
#
plc = factory.addPLC("Tgt-HeC1010:Ctrl-PLC-001")
# Properties
plc.setProperty("Hostname", "null")
plc.setProperty("EPICSModule", "[]")
plc.setProperty("EPICSSnippet", "[]")
plc.setProperty("PLCF#PLC-EPICS-COMMS:Endianness", "BigEndian")
plc.setProperty("PLCF#PLC-DIAG:Max-IO-Devices", "1")
plc.setProperty("PLCF#PLC-DIAG:Max-Modules-In-IO-Device", "30")
plc.setProperty("PLCF#EPICSToPLCDataBlockStartOffset", "0")
plc.setProperty("PLCF#PLCToEPICSDataBlockStartOffset", "0")
plc.setProperty("PLCF#PLC-EPICS-COMMS: MBConnectionID", "255")
plc.setProperty("PLCF#PLC-EPICS-COMMS: MBPort", "502")
plc.setProperty("PLCF#PLC-EPICS-COMMS: S7ConnectionID", "256")
plc.setProperty("PLCF#PLC-DIAG:Max-Local-Modules", "30")
plc.setProperty("PLCF#PLC-EPICS-COMMS: InterfaceID", "72")
plc.setProperty("PLCF#PLC-EPICS-COMMS: S7Port", "503")
plc.setProperty("PLCF#PLC-EPICS-COMMS: PLCPulse", "Pulse_200ms")
plc.setProperty("PLCF#PLC-EPICS-COMMS: DiagPort", "2001")
plc.setProperty("PLCF#PLC-EPICS-COMMS: DiagConnectionID", "254")
plc.setProperty("PLC-EPICS-COMMS: GatewayDatablock", "null")
# External links
plc.addLink("EPI[def/thccs_plc]", "https://gitlab.esss.lu.se/icshwi/thccs.git")

#
# Adding device Tgt-HeC1010:Proc-TT-021 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-TT-021")
# Properties
dev.setProperty("EGU", "C")

#
# Adding device Tgt-HeC1010:Proc-TT-022 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-TT-022")
# Properties
dev.setProperty("EGU", "C")

#
# Adding device Tgt-HeC1010:Proc-PT-011 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-PT-011")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeC1010:Proc-PT-012 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-PT-012")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeC1010:Proc-YSV-005a of type ICS_PV_AUMA
#
dev = plc.addDevice("ICS_PV_AUMA", "Tgt-HeC1010:Proc-YSV-005a")

#
# Adding device Tgt-HeC1010:Proc-YSV-005b of type ICS_PV_AUMA
#
dev = plc.addDevice("ICS_PV_AUMA", "Tgt-HeC1010:Proc-YSV-005b")

#
# Adding device Tgt-HeC1010:Proc-PT-014 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-PT-014")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeC1010:Proc-PT-015 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-PT-015")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeC1010:Proc-FT-001a of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-FT-001a")
# Properties
dev.setProperty("EGU", "kg/s")

#
# Adding device Tgt-HeC1010:Proc-FT-001b of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-FT-001b")
# Properties
dev.setProperty("EGU", "kg/s")

#
# Adding device Tgt-HeC1010:Proc-TT-012 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-TT-012")
# Properties
dev.setProperty("EGU", "C")

#
# Adding device Tgt-HeC1010:Proc-V-001a of type THCCS_Circulator
#
dev = plc.addDevice("THCCS_Circulator", "Tgt-HeC1010:Proc-V-001a")

#
# Adding device Tgt-HeC1010:Proc-V-001b of type THCCS_Circulator
#
dev = plc.addDevice("THCCS_Circulator", "Tgt-HeC1010:Proc-V-001b")

#
# Adding device Tgt-HeC1010:Proc-PT-001 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-PT-001")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeC1010:Proc-PT-002 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-PT-002")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeC1010:Proc-PT-003 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-PT-003")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeC1010:Proc-RT-002b of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-RT-002b")
# Properties
dev.setProperty("EGU", "Sv")

#
# Adding device Tgt-HeC1010:Proc-FIC-001 of type ICS_PID_ADV
#
dev = plc.addDevice("ICS_PID_ADV", "Tgt-HeC1010:Proc-FIC-001")

#
# Adding device Tgt-HeC1010:Proc-TT-011 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-TT-011")
# Properties
dev.setProperty("EGU", "C")

#
# Adding device Tgt-HeC1010:Proc-TT-002 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-TT-002")
# Properties
dev.setProperty("EGU", "C")

#
# Adding device Tgt-HeC1010:Proc-TT-003 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-TT-003")
# Properties
dev.setProperty("EGU", "C")

#
# Adding device Tgt-HeC1010:Proc-TT-004 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-TT-004")
# Properties
dev.setProperty("EGU", "C")

#
# Adding device Tgt-HeC1010:Proc-TT-005 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-TT-005")
# Properties
dev.setProperty("EGU", "C")

#
# Adding device Tgt-HeC1010:Proc-PT-004 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-PT-004")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeC1010:Proc-PT-005 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-PT-005")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeC1010:Proc-PT-006 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-PT-006")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeC1010:Proc-PT-007 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-PT-007")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeC1010:Proc-PT-008 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-PT-008")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeC1010:Proc-RT-001 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-RT-001")
# Properties
dev.setProperty("EGU", "Sv")

#
# Adding device Tgt-HeC1010:Proc-RT-002a of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-RT-002a")
# Properties
dev.setProperty("EGU", "Sv")

#
# Adding device Tgt-HeC1010:Proc-RT-003 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-RT-003")
# Properties
dev.setProperty("EGU", "Sv")

#
# Adding device Tgt-HeC1010:Proc-YSV-001 of type ICS_PV_AUMA
#
dev = plc.addDevice("ICS_PV_AUMA", "Tgt-HeC1010:Proc-YSV-001")

#
# Adding device Tgt-HeC1010:Proc-YSV-002 of type ICS_PV_AUMA
#
dev = plc.addDevice("ICS_PV_AUMA", "Tgt-HeC1010:Proc-YSV-002")

#
# Adding device Tgt-HeC1010:Proc-YSV-004 of type ICS_PV_AUMA
#
dev = plc.addDevice("ICS_PV_AUMA", "Tgt-HeC1010:Proc-YSV-004")

#
# Adding device Tgt-HeC1010:Proc-YSV-006 of type ICS_PV_AUMA
#
dev = plc.addDevice("ICS_PV_AUMA", "Tgt-HeC1010:Proc-YSV-006")

#
# Adding device Tgt-HeC1010:Proc-FCV-001 of type ICS_CV
#
dev = plc.addDevice("ICS_CV", "Tgt-HeC1010:Proc-FCV-001")

#
# Adding device Tgt-HeC1010:Proc-FT-002 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeC1010:Proc-FT-002")
# Properties
dev.setProperty("EGU", "m3/h")

#
# Adding device Tgt-HePC1011:Proc-PT-010 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HePC1011:Proc-PT-010")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HePC1011:Proc-PT-020a of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HePC1011:Proc-PT-020a")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HeIn1013:Proc-PT-033 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HeIn1013:Proc-PT-033")
# Properties
dev.setProperty("EGU", "bar")

#
# Adding device Tgt-HePu1015:Proc-TT-102 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HePu1015:Proc-TT-102")
# Properties
dev.setProperty("EGU", "C")

#
# Adding device Tgt-HePu1015:Proc-TT-503 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HePu1015:Proc-TT-503")
# Properties
dev.setProperty("EGU", "C")

#
# Adding device Tgt-HePu1015:Proc-TT-513 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HePu1015:Proc-TT-513")
# Properties
dev.setProperty("EGU", "C")

#
# Adding device Tgt-HePu1015:Proc-TT-404 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HePu1015:Proc-TT-404")
# Properties
dev.setProperty("EGU", "C")

#
# Adding device Tgt-HePu1015:Proc-FT-403 of type ICS_ANALOG
#
dev = plc.addDevice("ICS_ANALOG", "Tgt-HePu1015:Proc-FT-403")
# Properties
dev.setProperty("EGU", "m3/h")

#
# Adding device Tgt-HePu1015:Proc-YSV-101 of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HePu1015:Proc-YSV-101")

#
# Adding device Tgt-HePu1015:Proc-YSV-405 of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HePu1015:Proc-YSV-405")

#
# Adding device Tgt-HePu1015:Proc-YSV-120 of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HePu1015:Proc-YSV-120")

#
# Adding device Tgt-HePC1011:Proc-YSV-010 of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HePC1011:Proc-YSV-010")

#
# Adding device Tgt-HePC1011:Proc-YSV-012 of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HePC1011:Proc-YSV-012")

#
# Adding device Tgt-HePC1011:Proc-YSV-013 of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HePC1011:Proc-YSV-013")

#
# Adding device Tgt-HePC1011:Proc-YSV-016 of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HePC1011:Proc-YSV-016")

#
# Adding device Tgt-HePC1011:Proc-YSV-018 of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HePC1011:Proc-YSV-018")

#
# Adding device Tgt-HePC1011:Proc-YSV-021 of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HePC1011:Proc-YSV-021")

#
# Adding device Tgt-HePC1011:Proc-YSV-022a of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HePC1011:Proc-YSV-022a")

#
# Adding device Tgt-HePC1011:Proc-YSV-023a of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HePC1011:Proc-YSV-023a")

#
# Adding device Tgt-HePC1011:Proc-PCV-014 of type ICS_CV
#
dev = plc.addDevice("ICS_CV", "Tgt-HePC1011:Proc-PCV-014")

#
# Adding device Tgt-HePC1011:Proc-PCV-015 of type ICS_CV
#
dev = plc.addDevice("ICS_CV", "Tgt-HePC1011:Proc-PCV-015")

#
# Adding device Tgt-HeIn1013:Proc-PCV-033 of type ICS_CV
#
dev = plc.addDevice("ICS_CV", "Tgt-HeIn1013:Proc-PCV-033")

#
# Adding device Tgt-HePC1011:Proc-YSV-011 of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HePC1011:Proc-YSV-011")

#
# Adding device Tgt-HeIn1013:Proc-YSV-033 of type ICS_PV
#
dev = plc.addDevice("ICS_PV", "Tgt-HeIn1013:Proc-YSV-033")

#
# Adding device Tgt-HePC1011:Proc-V-011 of type THCCS_Circulator
#
dev = plc.addDevice("THCCS_Circulator", "Tgt-HePC1011:Proc-V-011")

#
# Adding device Tgt-HePC1011:Proc-PIC-011 of type ICS_PID
#
dev = plc.addDevice("ICS_PID", "Tgt-HePC1011:Proc-PIC-011")

#
# Adding device Tgt-HePu1015:Proc-PCV-203 of type ICS_CV
#
dev = plc.addDevice("ICS_CV", "Tgt-HePu1015:Proc-PCV-203")

#
# Adding device Tgt-HeC1010:Proc-FIC-002 of type ICS_PID_ADV
#
dev = plc.addDevice("ICS_PID_ADV", "Tgt-HeC1010:Proc-FIC-002")


#
# Saving the created CCDB
#
factory.save("Tgt-HeC1010:Ctrl-PLC-001")

import argparse

from petenv.opc_client import OPCClient

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Simulator")
    parser.add_argument("--ip", type=str, help="plc ip address", default="172.30.4.163")
    parser.add_argument("bp", type=float, help="beam power in MW")
    args = parser.parse_args()

    if not args.ip:
        args.ip = "172.30.4.163"
        print("using default ip {}".format(args.ip))

    # Create and connect client
    client = OPCClient(args.ip)
    client.connect()

    # Get all TT, PT and RT nodes
    objects = client.get_root_node().get_child("0:Objects").get_children()
    plc = objects[-1]                    # Node for PLC

    bp = client.get_root_node().get_child(
        ['0:Objects', '3:THCCS_PLC', '3:DataBlocksGlobal', '3:external_signals',
             '3:beam_power_mw'])

    client.setValue(bp, args.bp)

    client.disconnect()

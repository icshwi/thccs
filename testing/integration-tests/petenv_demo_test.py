import sys
import time

from petenv.opc_client import OPCClient
import pytest
# from py.xml import html

from epics import caget, caput

primary = [1, 0]

@pytest.fixture(scope="module")
def client():
    """Setup OPCUA client.

    Create OPCUA client and connect to PLC upon starting test. When the
    test is finished, disconnect the client.
    """
    c = OPCClient("172.30.4.163")
    c.connect()
    yield c  # Provide the fixture value. Eferything after this is teardown code
    sys.stdout.write("\nteardown client")
    c.disconnect()

def get_ysv_open_node(client, train):
    """ Returns valve_open OPCUA node.

    Args:
        client (OPCClient): OPCUA client
        train (str): "a" or "b"
    """
    return client.get_root_node().get_child([
        "0:Objects",
        "3:THCCS_PLC",
        "3:Inputs",
        "3:hwi_YSV-005{}_opened" .format(train)])

def get_circulator_state(client, train):
    """ Returns circulator_state OPCUA node.

    Args:
        client (OPCClient): OPCUA client
        train (str): "a" or "b"
    """
    return client.get_root_node().get_child([
        '0:Objects',
        '3:THCCS_PLC',
        '3:DataBlocksInstance',
        '3:V-001{}' .format(train),
        '3:Static',
        '3:op_state'])

def set_beam_power(client, bp):
    """ Sets beam power.

    Args:
        client (OPCClient): OPCUA client
        bp (float): Beam power
    """
    node = client.get_root_node().get_child([
        '0:Objects',
        '3:THCCS_PLC',
        '3:DataBlocksGlobal',
        '3:external_signals',
        '3:beam_power_mw'
        ])

    client.setValue(node, bp)

def standby():
    """Stop system.

    This method serves the purpose of stopping the system, and is called
    from multiple test functions.
    """
    running = 300
    stopping = 400
    standby = 100
    if caget("Tgt-HeC1010:Ctrl-PLC-001:FB_State") == running:
        caput("Tgt-HeC1010:Ctrl-PLC-001:P_State", stopping) # Go to STOPPING
    else:
        caput("Tgt-HeC1010:Ctrl-PLC-001:P_State", 100) # Go to STOPPING

    while caget("Tgt-HeC1010:Ctrl-PLC-001:FB_State") != standby:
        time.sleep(0.5)

def test_modes():
    """Verify operational modes of devices"""
    modes = ["Manual", "Force", "Auto"]
    devices = ["V-001a", "V-001a", "YSV-005a", "YSV-005b"]
    for m in modes:
        for d in devices:
            # Set device to mode
            caput("Tgt-HeC1010:Proc-{}:Cmd_{}".format(d, m), 1)

            # Wait for commands to take effect
            time.sleep(1)

            # If in mode is forced, add the 'd' to the mode for verification
            v_mode = m
            if v_mode == "Force":
                v_mode += "d"

            # Verify mode
            assert caget("Tgt-HeC1010:Proc-{}:OpMode_{}".format(d, v_mode)) == 1

@pytest.mark.parametrize("prim", primary, ids=["V-001a", "V-001b"])
def test_circulator_startup_sequence(client, prim):
    """Verify single circulator startup sequence.

    Go to RUNNING with beam power = 0, implying that only the primary
    circulator shall start. Run test with each circulator as primary.

    Args:
        client (OPCClient): OPCUA client
        prim (int): Indicating which circulator is primary (1=A, 0=B)
    """
    # Go to standby
    standby()

    # Verify that we are starting in OFF or STANDBY state
    assert caget("Tgt-HeC1010:Ctrl-PLC-001:FB_State") in [0, 100]

    # Set beam power to 0
    set_beam_power(client, 0)

    # Assign circulators as primary and secondary respectively.
    caput("Tgt-HeC1010:Proc-V-001a:P_Primary", prim)
    caput("Tgt-HeC1010:Proc-V-001b:P_Primary", 1-prim)

    # Step up state machine to RUNNING
    caput("Tgt-HeC1010:Ctrl-PLC-001:P_State", 200)
    while caget("Tgt-HeC1010:Ctrl-PLC-001:FB_State") != 300:
        time.sleep(0.2)

    # Store relevant valve and circulator nodes in a lists
    YSV = [get_ysv_open_node(client, "a"), get_ysv_open_node(client, "b")]
    V = [get_circulator_state(client, "a"), get_circulator_state(client, "b")]

    # While the primary valve is not open
    t = 0.0
    while not YSV[1-prim].get_value():
        assert V[0].get_value() == 0 # Assert circulator A is off
        assert V[1].get_value() == 0 # Assert circulator B is off
        assert not YSV[prim].get_value() # Assert secondary valve is off
        time.sleep(0.2)
        t += 0.2
        if t > 30: # break while after 30s if while loop is still running
            break

    # While primary circulator is not running
    while V[1-prim].get_value() != 2:
        assert V[prim].get_value() == 0 # Assert other circulator is off
        assert not YSV[prim].get_value() # Assert other valve is not open
        time.sleep(0.2)

    assert V[prim].get_value() == 0 # Assert other circulator is still off
    assert not YSV[prim].get_value() # Assert other valve still is not open

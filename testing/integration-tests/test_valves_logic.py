import inspect
import sys
import time

from petenv.opc_client import OPCClient
import pytest
from py.xml import html

from epics import ca, caget, caput

QUIET = True
VERBOSE = True

def quiet_mode(msg_bytes):
    """Quench pyepics warnings

    When running over VPN, two networks are enabled. The client library
    will find the PV on the same IOC twice, from different IP
    addresses. This ambiguity results in the following warning:

    CA.Client.Exception...............................................
        Warning: "Identical process variable names on multiple servers"
        Context: "Channel: "Tgt-HeIn1013:Proc-YSV-033:ClosingTime",
        Connecting to: L480:35519, Ignored: L480:35519"
        Source File: ../cac.cpp line 1320
        Current Time: Fri Mar 20 2020 10:51:04.209830858
    ..................................................................

    As this is no problem for this test, this function serves the
    purpose to quench such warnings in order to not clutter the pytest
    terminal output.
    """
    pass  # Alternatively, we could write the warnings into a log file.


if QUIET:
    ca.replace_printf_handler(quiet_mode)  # Replace pyepics handler

def DEBUG(msg):
    """Returns the current line number in our program."""
    if VERBOSE:
        print("line {}: {}".format(inspect.currentframe().f_back.f_lineno, msg))

@pytest.fixture(scope="module")
def client():
    """Setup OPCUA client.

    Create OPCUA client and connect to PLC upon starting test. When the
    test is finished, disconnect the client.
    """
    c = OPCClient("172.30.4.163")
    c.connect()

    yield c  # Provide the fixture value. Eferything after this is teardown code
    sys.stdout.write("\nteardown client")
    c.disconnect()

# @pytest.mark.skip(reason="already works")
@pytest.mark.parametrize("valve", ["YSV-001", "YSV-002", "YSV-004"])
def test_logic1(client, valve):
    """ Test logic for valves YSV-001, YSV-002 and YSV-004

    Args:
        client (OPCClient): OPCUA client
        valve (str): P&ID tag of the valve to test on
    """
    # Start tests in machine state off
    caput("Tgt-HeC1010:Ctrl-PLC-001:P_State", 0)
    wait("Tgt-HeC1010:Ctrl-PLC-001:FB_State", 0)
    assert caget("Tgt-HeC1010:Ctrl-PLC-001:FB_State") == 0
    # __________________________________________________________________________

    # Go to machine state maintenance
    # Expected: Valve goes into manual mode

    # Set auto mode and wait for it to take effect
    caput("Tgt-HeC1010:Proc-{}:Cmd_Auto".format(valve), 1)
    wait("Tgt-HeC1010:Proc-{}:OpMode_Auto".format(valve), 1)
    assert caget("Tgt-HeC1010:Proc-{}:OpMode_Auto".format(valve)) == 1

    # Go to machine state maintenance and wait for state to be reached
    caput("Tgt-HeC1010:Ctrl-PLC-001:P_State", 500)
    wait("Tgt-HeC1010:Ctrl-PLC-001:FB_State", 500)
    assert caget("Tgt-HeC1010:Ctrl-PLC-001:FB_State") == 500

    # Wait for valve to change state and assert that it is now in manual
    wait("Tgt-HeC1010:Proc-{}:OpMode_Manual".format(valve), 1)
    assert caget("Tgt-HeC1010:Proc-{}:OpMode_Manual".format(valve)) == 1
    # __________________________________________________________________________

    # Go to machine state off
    # Expected: Valve opens

    # Close the valve
    caput("Tgt-HeC1010:Proc-{}:Cmd_ManuClose".format(valve), 1)
    close_time = float(caget("Tgt-HeC1010:Proc-{}:ClosingTime".format(valve)))/1000
    wait("Tgt-HeC1010:Proc-{}:Closed".format(valve), 1, close_time)
    assert caget("Tgt-HeC1010:Proc-{}:Closed".format(valve)) == 1

    # Go to machine state off
    caput("Tgt-HeC1010:Ctrl-PLC-001:P_State", 0)
    wait("Tgt-HeC1010:Ctrl-PLC-001:FB_State", 0) # Wait until state is reached

    # Wait for valve to open (allow for same time as the time for the valve)
    open_time = float(caget("Tgt-HeC1010:Proc-{}:OpeningTime".format(valve)))/1000
    wait("Tgt-HeC1010:Proc-{}:Opened".format(valve), 1, open_time)

    # Assert that valve is open
    assert caget("Tgt-HeC1010:Proc-{}:Opened".format(valve)) == 1
    # __________________________________________________________________________

    # Go to machine state fault
    # Expected: Valve goes into manual mode

    # Set auto mode and wait for it to take effect
    caput("Tgt-HeC1010:Proc-{}:Cmd_Auto".format(valve), 1)
    wait("Tgt-HeC1010:Proc-{}:OpMode_Auto".format(valve), 1)
    assert caget("Tgt-HeC1010:Proc-{}:OpMode_Auto".format(valve)) == 1

    # Go to machine state fault and wait for state to be reached (note that we
    # must first go to standby as fault is not reachable from off)
    caput("Tgt-HeC1010:Ctrl-PLC-001:P_State", 100)
    wait("Tgt-HeC1010:Ctrl-PLC-001:FB_State", 100)
    caput("Tgt-HeC1010:Ctrl-PLC-001:P_State", 900)
    wait("Tgt-HeC1010:Ctrl-PLC-001:FB_State", 900)
    assert caget("Tgt-HeC1010:Ctrl-PLC-001:FB_State") == 900

    # Wait for valve to change state and assert that it is now in manual
    wait("Tgt-HeC1010:Proc-{}:OpMode_Manual".format(valve), 1)
    assert caget("Tgt-HeC1010:Proc-{}:OpMode_Manual".format(valve)) == 1

# @pytest.mark.skip(reason="already works")
@pytest.mark.parametrize("valve", ["YSV-006"])
def test_logic2(client, valve):
    """ Test logic for valve YSV-006

    Args:
        client (OPCClient): OPCUA client
        valve (str): P&ID tag of the valve to test on
    """
    # Start tests in machine state off
    caput("Tgt-HeC1010:Ctrl-PLC-001:P_State", 0)
    wait("Tgt-HeC1010:Ctrl-PLC-001:FB_State", 0)
    assert caget("Tgt-HeC1010:Ctrl-PLC-001:FB_State") == 0
    # __________________________________________________________________________

    # Go to machine state maintenance substate drain
    # Expected: Valve goes into auto mode and closes


def wait(pv, value, timeout=4.0):
    """Wait for PV value.

    Run `caget` on a PV until expected value is seen, or until time.

    Args:
        pv (str): EPICS PV name
        value (float): Expected PV value
        time (float): Timeout in seconds
    """
    t = 0
    delay = 0.2
    while caget(pv) != value:
        t += 1
        time.sleep(delay)
        if t > timeout / delay:
            break

import argparse
from petenv.opc_client import OPCClient
from epics import caget, caput

def set_mode(mode):
    mode = mode.title()
    client = OPCClient("172.30.4.163")
    client.connect()
    objects = client.get_root_node().get_child("0:Objects").get_children()
    plc = objects[-1]                    # Node for PLC
    instances = plc.get_child('3:DataBlocksInstance').get_children()

    for node in instances:
        node_name = node.get_display_name().Text
        if "DEV_" in node_name and "V-" in node_name:
            cmd = "{}:Cmd_{}" .format(node_name.split("_")[1], mode)
            caput(cmd, 1)

    client.disconnect()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="mode")
    parser.add_argument("mode", type=str, help="Mode to set field objects to")
    args = parser.parse_args()

    set_mode(args.mode)

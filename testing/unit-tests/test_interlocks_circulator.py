import inspect
import sys
import time

from petenv.opc_client import OPCClient
import pytest
from py.xml import html

from epics import ca, caget, caput

QUIET = True
VERBOSE = True

def quiet_mode(msg_bytes):
    """Quench pyepics warnings

    When running over VPN, two networks are enabled. The client library will
    find the PV on the same IOC twice, from different IP addresses. This ambiguity
    results in the following warning:

    CA.Client.Exception...............................................
        Warning: "Identical process variable names on multiple servers"
        Context: "Channel: "Tgt-HeIn1013:Proc-YSV-033:ClosingTime",
        Connecting to: L480:35519, Ignored: L480:35519"
        Source File: ../cac.cpp line 1320
        Current Time: Fri Mar 20 2020 10:51:04.209830858
    ..................................................................

    As this is no problem for this test, this function serves the
    purpose to quench such warnings in order to not clutter the pytest
    terminal output.
    """
    pass  # Alternatively, we could write the warnings into a log file.


if QUIET:
    ca.replace_printf_handler(quiet_mode)  # Replace pyepics handler

MIN_RPM = 10000
order = [("a","b"), ("b", "a")]

# Set this flag to True to print debug messages

def DEBUG(msg):
    """Returns the current line number in our program."""
    if VERBOSE:
        print("line {}: {}".format(inspect.currentframe().f_back.f_lineno, msg))

@pytest.fixture(scope="module")
def client():
    """Setup OPCUA client.

    Create OPCUA client and connect to PLC upon starting test. When the
    test is finished, disconnect the client.
    """
    c = OPCClient("opc.tcp://172.30.4.163")
    c.connect()
    stop()

    yield c  # Provide the fixture value. Eferything after this is teardown code
    sys.stdout.write("\nteardown client")
    stop()
    c.disconnect()

# V-001a/b
# @pytest.mark.skip(reason="save time")
@pytest.mark.parametrize("prim, sec", order, ids=["Train A", "Train B"])
def test_start_closed(prim, sec):
    """Start circulator with its trailing valve closed.

    This test verifies the interlock preventing running circulator with
    closed valve.
    """
    init()
    assert caget("Tgt-HeC1010:Proc-YSV-005{}:Closed" .format(prim)) == 1
    caput("Tgt-HeC1010:Proc-V-001{}:P_Setpoint" .format(prim), MIN_RPM+2000)
    caput("Tgt-HeC1010:Proc-V-001{}:Cmd_Start" .format(prim), 1)
    time.sleep(5)
    assert caget("Tgt-HeC1010:Proc-V-001{}:OpState" .format(prim)) == 0

# @pytest.mark.skip(reason="save time")
def test_stop_with_rpm_violation():
    init()
    # Open valves
    DEBUG("Opening valves")
    caput("Tgt-HeC1010:Proc-YSV-005a:Cmd_ManuOpen", 1)
    caput("Tgt-HeC1010:Proc-YSV-005b:Cmd_ManuOpen", 1)

    # Wait for valves to be open
    wait("Tgt-HeC1010:Proc-YSV-005a:Opened", 1)
    wait("Tgt-HeC1010:Proc-YSV-005b:Opened", 1)

    # Wait to make sure the interlock is revoked
    time.sleep(2)

    # Set circulators to manual mode
    DEBUG("Setting circulators to manual mode")
    caput("Tgt-HeC1010:Proc-V-001a:Cmd_Manual", 1)
    caput("Tgt-HeC1010:Proc-V-001b:Cmd_Manual", 1)

    # Specify a setpoint greater than min_rpm
    sp = MIN_RPM+2000
    DEBUG("Setting setpoint to {} rpm".format(sp))
    caput("Tgt-HeC1010:Proc-V-001a:P_Setpoint", sp)

    # Start circulators
    DEBUG("Starting circulators")
    caput("Tgt-HeC1010:Proc-V-001a:Cmd_Start", 1)
    caput("Tgt-HeC1010:Proc-V-001b:Cmd_Start", 1)

    # Wait for circulators to reach setpoint
    wait("Tgt-HeC1010:Proc-V-001a:Speed", sp, 100)
    wait("Tgt-HeC1010:Proc-V-001b:Speed", sp, 100)

    # Assert that the speed = setpoint
    speed_a = caget("Tgt-HeC1010:Proc-V-001a:Speed")
    speed_b = caget("Tgt-HeC1010:Proc-V-001b:Speed")
    assert speed_a == sp
    assert speed_b == sp

    # Send stop command to A
    DEBUG("Sending stop command to A")
    caput("Tgt-HeC1010:Proc-V-001a:Cmd_Stop", 1)

    # Wait for stop command to have effect
    time.sleep(5)

    # Assert that speed did not change (as it should be interlocked)
    assert caget("Tgt-HeC1010:Proc-V-001a:Speed") == sp

    # Send stop command to B
    caput("Tgt-HeC1010:Proc-V-001b:Cmd_Stop", 1)

    # Wait for stop command to have effect
    time.sleep(5)

    # Assert that speed did not change (as it should be interlocked)
    DEBUG("Sending stop command to B")
    assert caget("Tgt-HeC1010:Proc-V-001b:Speed") == sp

# YSV-005a/b
# @pytest.mark.skip(reason="save time")
@pytest.mark.parametrize("prim, sec", order, ids=["Train A", "Train B"])
def test_open_while_startingstopping(prim, sec):
    """Open valve while other circulator starting.

    This test verifies the interlock preventing opening the valve while
    the other circulator is starting and stopping..
    """
    init()
    time.sleep(1)

    # Open primary valve
    DEBUG("Open primary valve")
    caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_ManuOpen" .format(prim), 1)
    wait("Tgt-HeC1010:Proc-YSV-005{}:Opened" .format(prim), 1)

    # Start primary circulator
    DEBUG("Start primary circulator")
    caput("Tgt-HeC1010:Proc-V-001{}:P_Setpoint" .format(prim), MIN_RPM)
    caput("Tgt-HeC1010:Proc-V-001{}:Cmd_Start" .format(prim), 1)

    # Wait for circulator to be in starting
    DEBUG("Wait for circulator to be in starting")
    wait("Tgt-HeC1010:Proc-V-001{}:OpState".format(prim), 11) # 11 = starting
    state = caget("Tgt-HeC1010:Proc-V-001{}:OpState".format(prim))
    DEBUG("state {}".format(state))
    assert caget("Tgt-HeC1010:Proc-V-001{}:OpState" .format(prim)) == 11

    # Attempt to open other valve (shouldn't work)
    DEBUG("Attempt to open valve {} (shouldn't work)".format(sec))
    caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_ManuOpen" .format(sec), 1)
    wait("Tgt-HeC1010:Proc-YSV-005{}:Opened".format(sec), 1)
    assert caget("Tgt-HeC1010:Proc-YSV-005{}:Closed" .format(sec)) == 1

    # Wait for circulator to reach running
    wait("Tgt-HeC1010:Proc-V-001{}:OpState".format(prim), 2) # 2 = running
    assert caget("Tgt-HeC1010:Proc-V-001{}:OpState".format(prim)) == 2

    # Stop circulator
    DEBUG("Stop circulator")
    caput("Tgt-HeC1010:Proc-V-001{}:Cmd_Stop" .format(prim), 1)
    wait("Tgt-HeC1010:Proc-V-001{}:OpState" .format(prim), 4, 20)
    assert caget("Tgt-HeC1010:Proc-V-001{}:OpState" .format(prim)) == 4

    # Try to open valve while circulator is starting (shouldn't work)
    DEBUG("Attempt to open valve {} (shouldn't work)".format(sec))
    caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_ManuOpen" .format(sec), 1)
    wait("Tgt-HeC1010:Proc-YSV-005{}:Opened".format(sec), 1)
    assert caget("Tgt-HeC1010:Proc-YSV-005{}:Closed" .format(sec)) == 1

# @pytest.mark.skip(reason="save time")
@pytest.mark.parametrize("prim, sec", order, ids=["Train A", "Train B"])
def test_close_while_working(prim, sec):
    """Close valve while leading circulator is starting, running and stopping.

    This test verifies the interlock preventing closing the valve as
    long as the circulator is not off. The test tries to put the valve
    in manual mode and command it to close. It is expected that the valve
    is constantly in auto mode and cannot be closed while interlocked.
    """
    init()

    # Open primary valve
    DEBUG("Opening primary valve")
    caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_Manual" .format(prim), 1)
    caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_ManuOpen" .format(prim), 1)
    wait("Tgt-HeC1010:Proc-YSV-005{}:Opened" .format(prim), 1)
    assert caget("Tgt-HeC1010:Proc-YSV-005{}:Opened" .format(prim)) == 1

    # Set setpoint to 12000 and start circulator
    DEBUG("Setting setpoint to {} rpm".format(MIN_RPM+2000))
    caput("Tgt-HeC1010:Proc-V-001{}:P_Setpoint" .format(prim), MIN_RPM+2000)
    DEBUG("Starting primary circulator")
    caput("Tgt-HeC1010:Proc-V-001{}:Cmd_Start" .format(prim), 1)
    wait("Tgt-HeC1010:Proc-V-001{}:OpState" .format(prim), 11)
    DEBUG("Primary circulator is in starting")

    # Attempt to close valve during circulator startup
    DEBUG("Telling primary valve to close")
    caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_ManuClose" .format(prim), 1)

    wait("Tgt-HeC1010:Proc-V-001{}:OpState" .format(prim), 2)

    DEBUG("Assert that valve did not close during circulator starting phase")
    assert caget("Tgt-HeC1010:Proc-YSV-005{}:Opened" .format(prim)) == 1

    # Attempt to close valve during circulator running
    DEBUG("Telling primary valve to close")
    caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_ManuClose" .format(prim), 1)
    time.sleep(5)
    DEBUG("Assert that valve did not close during circulator running phase")
    assert caget("Tgt-HeC1010:Proc-YSV-005{}:Opened" .format(prim)) == 1

    # Stop circulator and assert that valve cannot be closed during stopping
    DEBUG("Stop primary circulator")
    caput("Tgt-HeC1010:Proc-V-001{}:P_Setpoint" .format(prim), MIN_RPM)
    wait("Tgt-HeC1010:Proc-V-001{}:Speed" .format(prim), MIN_RPM)
    caput("Tgt-HeC1010:Proc-V-001{}:Cmd_Stop" .format(prim), 1)
    wait("Tgt-HeC1010:Proc-V-001{}:OpState" .format(prim), 4)
    while caget("Tgt-HeC1010:Proc-V-001{}:OpState" .format(prim)) != 0:
        DEBUG("Telling primary valve to close")
        caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_ManuClose" .format(prim), 1)
        DEBUG("Asserting that valve did not close")
        assert caget("Tgt-HeC1010:Proc-YSV-005{}:Opened" .format(prim)) == 1
        time.sleep(0.5)

# @pytest.mark.skip(reason="save time")
@pytest.mark.parametrize("prim, sec", order, ids=["Train A", "Train B"])
def test_violate_min_rpm(prim, sec):
    """Open valve while other circulator is running faster than min_rpm.

    This test verifies the interlock preventing opening the valve. Test
    also verifies that opening valve when other circulator runs at
    min_rpm is working.
    """
    init()
    DEBUG(prim)
    # Open valve and ramp up circulator to min_rpm
    DEBUG("Open valve and ramp up circulator to min_rpm")
    caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_ManuOpen".format(prim), 1)
    wait("Tgt-HeC1010:Proc-YSV-005{}:Opened".format(prim), 1)

    # Set setpoint to min rpm and start primary circulator
    DEBUG("Set setpoint to min rpm and start primary circulator")
    sp = MIN_RPM
    caput("Tgt-HeC1010:Proc-V-001{}:P_Setpoint" .format(prim), sp)
    caput("Tgt-HeC1010:Proc-V-001{}:Cmd_Start" .format(prim), 1)

    # Wait until circulator is in state RUNNING and speed as achieved
    DEBUG("Wait until circulator is in state RUNNING and speed as achieved")
    wait("Tgt-HeC1010:Proc-V-001{}:OpState" .format(prim), 2, 100)
    wait("Tgt-HeC1010:Proc-V-001{}:Speed" .format(prim), sp)

    # Assert that correct speed is reached
    DEBUG("Assert that correct speed is reached")
    assert caget("Tgt-HeC1010:Proc-V-001{}:Speed" .format(prim)) == sp

    # Verify that secondary valve can be opened and closed
    DEBUG("Verify that secondary valve can be opened and closed")
    caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_Manual" .format(sec), 1)
    caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_ManuOpen" .format(sec), 1)
    wait("Tgt-HeC1010:Proc-YSV-005{}:Opened".format(sec), 1)
    assert caget("Tgt-HeC1010:Proc-YSV-005{}:Opened" .format(sec)) == 1

    # Close secondary valve again
    DEBUG("Close secondary valve again")
    caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_ManuClose" .format(sec), 1)
    wait("Tgt-HeC1010:Proc-YSV-005{}:Closed".format(sec), 1)
    assert caget("Tgt-HeC1010:Proc-YSV-005{}:Closed" .format(sec)) == 1

    # Ramp up above min_rpm
    DEBUG("Ramp up above min_rpm")
    sp = MIN_RPM+2000
    caput("Tgt-HeC1010:Proc-V-001{}:P_Setpoint" .format(prim), sp)
    wait("Tgt-HeC1010:Proc-V-001{}:Speed" .format(prim), sp, 100)

    # Assert that correct speed is reached
    DEBUG("Assert that correct speed is reached")
    assert caget("Tgt-HeC1010:Proc-V-001{}:Speed" .format(prim)) == sp

    # Assert that secondary valve cannot be opened
    DEBUG("Assert that secondary valve cannot be opened")
    caput("Tgt-HeC1010:Proc-YSV-005{}:Cmd_ManuOpen" .format(sec), 1)
    time.sleep(5)
    assert caget("Tgt-HeC1010:Proc-YSV-005{}:Closed" .format(sec)) == 1

def init():
    """Start system.

    This method serves the purpose of getting ready to run the tests.
    """
    DEBUG("Initialize test")
    DEBUG("Go to mainenance")
    if caget("Tgt-HeC1010:Ctrl-PLC-001:FB_State") != 500:
        caput("Tgt-HeC1010:Ctrl-PLC-001:P_State", 500) # Go to MAINTENANCE
        wait("Tgt-HeC1010:Ctrl-PLC-001:FB_State", 500)

    DEBUG("Set all to manual")
    if caget("Tgt-HeC1010:Proc-V-001a:OpMode_Manual") != 1:
        caput("Tgt-HeC1010:Proc-V-001a:Cmd_Manual", 1)
        wait("Tgt-HeC1010:Proc-V-001a:OpMode_Manual", 1)
    if caget("Tgt-HeC1010:Proc-V-001b:OpMode_Manual") != 1:
        caput("Tgt-HeC1010:Proc-V-001b:Cmd_Manual", 1)
        wait("Tgt-HeC1010:Proc-V-001b:OpMode_Manual", 1)
    if caget("Tgt-HeC1010:Proc-YSV-005a:OpMode_Manual") != 1:
        caput("Tgt-HeC1010:Proc-YSV-005a:Cmd_Manual", 1)
        wait("Tgt-HeC1010:Proc-YSV-005a:OpMode_Manual", 1)
    if caget("Tgt-HeC1010:Proc-YSV-005b:OpMode_Manual") != 1:
        caput("Tgt-HeC1010:Proc-YSV-005b:Cmd_Manual", 1)
        wait("Tgt-HeC1010:Proc-YSV-005b:OpMode_Manual", 1)

    DEBUG("Shutdown circulators")
    if caget("Tgt-HeC1010:Proc-V-001a:OpState") != 0:
        DEBUG("Go to min rpm")
        caput("Tgt-HeC1010:Proc-V-001a:P_Setpoint", MIN_RPM)
        wait("Tgt-HeC1010:Proc-V-001a:Speed", MIN_RPM)
        DEBUG("Stop circulator")
        caput("Tgt-HeC1010:Proc-V-001a:Cmd_Stop", 1)
        wait("Tgt-HeC1010:Proc-V-001a:OpState", 0)
    if caget("Tgt-HeC1010:Proc-V-001b:OpState") != 0:
        DEBUG("Go to min rpm")
        caput("Tgt-HeC1010:Proc-V-001a:P_Setpoint", MIN_RPM)
        wait("Tgt-HeC1010:Proc-V-001b:Speed", MIN_RPM)
        DEBUG("Stop circulator")
        caput("Tgt-HeC1010:Proc-V-001b:Cmd_Stop", 1)
        wait("Tgt-HeC1010:Proc-V-001b:OpState", 0)

    DEBUG("Close valves")
    if caget("Tgt-HeC1010:Proc-YSV-005a:Closed") != 1:
        caput("Tgt-HeC1010:Proc-YSV-005a:Cmd_ManuClose", 1)
        wait("Tgt-HeC1010:Proc-YSV-005a:Closed", 1)
    if caget("Tgt-HeC1010:Proc-YSV-005b:Closed") != 1:
        caput("Tgt-HeC1010:Proc-YSV-005b:Cmd_ManuClose", 1)
        wait("Tgt-HeC1010:Proc-YSV-005b:Closed", 1)

    DEBUG("init done")

def stop():
    """Stop system.

    This method serves the purpose of stopping the system, and is called
    from multiple test functions.
    """
    if caget("Tgt-HeC1010:Ctrl-PLC-001:FB_State") == 0:
        return

    stop = 400
    caput("Tgt-HeC1010:Ctrl-PLC-001:P_State", stop) # Go to STOPPING

    while caget("Tgt-HeC1010:Ctrl-PLC-001:P_State") != stop:
        time.sleep(1)

    while caget("Tgt-HeC1010:Ctrl-PLC-001:FB_State") != 0:
        time.sleep(0.2)

def get_ysv_open_node(client, train):
    """ Returns valve_open OPCUA node.

    Args:
        client (OPCClient): OPCUA client
        train (str): "a" or "b"
    """
    return client.get_root_node().get_child([
        "0:Objects",
        "3:THCCS_PLC",
        "3:Inputs",
        "3:hwiYSV005{}Open" .format(train)])

def get_circulator_state(client, train):
    """ Returns circulator_state OPCUA node.

    Args:
        client (OPCClient): OPCUA client
        train (str): "a" or "b"
    """
    return client.get_root_node().get_child([
        '0:Objects',
        '3:THCCS_PLC',
        '3:DataBlocksInstance',
        '3:V{}-Layer0' .format(train),
        '3:Static',
        '3:op_state'])

def set_beam_power(client, bp):
    """ Sets beam power.

    Args:
        client (OPCClient): OPCUA client
        bp (float): Beam power
    """
    node = client.get_root_node().get_child([
        '0:Objects',
        '3:THCCS_PLC',
        '3:DataBlocksGlobal',
        '3:GlobalVars',
        '3:BeamPowerMW'])
    client.setValue(node, bp)

def set_mode(mode):
    mode = mode.title()
    client = OPCClient("172.30.4.163")
    client.connect()
    objects = client.get_root_node().get_child("0:Objects").get_children()
    plc = objects[-1]                    # Node for PLC
    instances = plc.get_child('3:DataBlocksInstance').get_children()

    for node in instances:
        node_name = node.get_display_name().Text
        if "DEV_" in node_name and "V-" in node_name:
            cmd = "{}:Cmd_{}" .format(node_name.split("_")[1], mode)
            caput(cmd, 1)

    client.disconnect()

def wait(pv, value, timeout=10.0):
    """Wait for PV value.

    Run `caget` on a PV until expected value is seen, or until timeout.

    Args:
        pv (str): EPICS PV name
        value (float): Expected PV value
        timeout (float): Timeout in seconds
    """
    t = 0
    delay = 0.2
    while caget(pv) != value:
        t += 1
        time.sleep(delay)
        if t > timeout / delay:
            break

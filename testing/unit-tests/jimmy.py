import pytest
from epics import caput,caget
import time

def test_add():
    caput("Tgt-HeC1010:Proc-YSV-001:Cmd_ManuOpen",1)
    time.sleep(5)
    assert caget("Tgt-HeC1010:Proc-YSV-001:Opened")
